export function AddNewsForm() {
  return (
    <form>
      <legend>Nueva noticia</legend>
      <div>
        <label>Titulo</label>
        <input />
      </div>
      <div>
        <label>Descripción</label>
        <textarea />
      </div>
      <div>
        <label>IFrame</label>
        <input />
      </div>
      <button>Guardar</button>
    </form>
  )
}
