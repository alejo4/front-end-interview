import news from '../data/news.json'

export function NewsList() {
  return (
    <main className="main-content" style={{width: '100%'}}>
      <pre>{JSON.stringify(news, null, 2)}</pre>
    </main>
  )
}
