import { Header } from './components/Header'
import { NewsList } from './components/NewsList'
// import { AddNewsForm } from './components/AddNewsForm'

import './App.css'

function App() {
  return (
    <div className="app">
      <Header />
      <div className="news-container">
        {/*<AddNewsForm />*/}
        <NewsList />
      </div>
    </div>
  )
}

export default App
