# Prueba Lapix

This is the application structure:

```
---------------------
| App.tsx           |
|   --------------  |
|   |  Header    |  |
|   --------------  |
|   --------------  |
|   |  NewsList  |  |
|   --------------  |
|--------------------
```

## Endpoints

**Base path**: https://interview.backend.lapix.com.co/api/v1

### List news

**Path**: /news

**Method**: GET

**Response**

```json
[
    {
        "id": "1",
        "title": "El dólar continúa con su senda alcista y supera los $4.100 después de dos semanas",
        "description": "El barril del petróleo Brent, de referencia para Colombia, subía 1,38%, a US$78 en la mañana, mientras que el WTI ganaba 1,26% a US$73","iframe":"","createdAt":"0001-01-01T00:00:00Z"
    }
]
```

### Create new record

**Path**: /news

**Method**: POST

**Request**
```json
{
    "title": "Los \"tokens\" y otras novedades que están mejorando los chatbots de inteligencia artificial",
    "description": "OpenAI presentó su nuevo modelo de chatbot, GPT-4 Turbo. Google anunció la integración de IA generativa a su buscador en inglés, español y portugués disponible en varios países de América Latina. Y Elon Musk entró a la contienda con su chatbot Grok, que será integrado a X (antes Twitter)."
    "iframe": "https://www.bbc.com/mundo/articles/crgp692j4ppo"
}
```

**Response**

```json
{
    "id": "1",
    "title": "Los \"tokens\" y otras novedades que están mejorando los chatbots de inteligencia artificial",
    "description": "OpenAI presentó su nuevo modelo de chatbot, GPT-4 Turbo. Google anunció la integración de IA generativa a su buscador en inglés, español y portugués disponible en varios países de América Latina. Y Elon Musk entró a la contienda con su chatbot Grok, que será integrado a X (antes Twitter)."
    "iframe": "https://www.bbc.com/mundo/articles/crgp692j4ppo"
    "createdAt": "0001-01-01T00:00:00Z"
}
```
